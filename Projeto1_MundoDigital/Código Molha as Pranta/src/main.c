
/**
 * 5 semestre - Eng. da Computao - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"

/************************************************************************/
/* defines                                                              */
/************************************************************************/

#define LED_VERMELHO_PIO PIOC
#define LED_VERMELHO_PIO_ID 12
#define LED_VERMELHO_PIO_PIN 30
#define LED_VERMELHO_PIO_PIN_MASK (1 << LED_VERMELHO_PIO_PIN)

#define LED_VERDE_PIO_ID ID_PIOD
#define LED_VERDE_PIO_PIN 22
#define LED_VERDE_PIO_PIN_MASK (1 << LED_VERDE_PIO_PIN)
#define LED_VERDE_PIO PIOD

#define BUT_PIO_ID 10
#define BUT_PIO_PIN 19
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)
#define BUT_PIO PIOA

#define VAL_PIO_ID ID_PIOA
#define VAL_PIO_PIN 4
#define VAL_PIO_PIN_MASK (1 << VAL_PIO_PIN)
#define VAL_PIO PIOA

#define SENSOR_PIO_ID ID_PIOA
#define SENSOR_PIO_PIN 3
#define SENSOR_PIO_PIN_MASK (1 << SENSOR_PIO_PIN)
#define SENSOR_PIO PIOA



/************************************************************************/
/* constants                                                            */
/************************************************************************/

/************************************************************************/
/* variaveis globais                                                    */
/************************************************************************/

/************************************************************************/
/* interrupcoes                                                         */
/************************************************************************/

/************************************************************************/
/* funcoes                                                              */
/************************************************************************/


volatile but_flag = false;
volatile acionamento = false;



void but_callBack(void){
  but_flag = true;
}

void ligaValvula(void){
	pio_set(VAL_PIO, VAL_PIO_PIN_MASK);
}

void desligaValvula(void){
	pio_clear(VAL_PIO, VAL_PIO_PIN_MASK);	
}

void ligaLED(Pio *p_pio, const uint32_t ul_mask){
	pio_clear(p_pio, ul_mask);
}

void desligaLED(Pio *p_pio, const uint32_t ul_mask){
	pio_set(p_pio, ul_mask);
}


/************************************************************************/
/* Main                                                                 */
/************************************************************************/

// Funcao principal chamada na inicalizacao do uC.
int main(void){
	
	sysclk_init();

	WDT->WDT_MR = WDT_MR_WDDIS;
	
	pmc_enable_periph_clk(BUT_PIO_ID);
	
    //Saidas
	pio_configure(LED_VERMELHO_PIO ,PIO_OUTPUT_0, LED_VERMELHO_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(LED_VERDE_PIO, PIO_OUTPUT_0, LED_VERDE_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(VAL_PIO, PIO_OUTPUT_0, VAL_PIO_PIN_MASK, PIO_DEFAULT);
	
    //Entradas
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP);
	pio_configure(SENSOR_PIO, PIO_INPUT, SENSOR_PIO_PIN_MASK, PIO_DEFAULT);
	
	//Comando
	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, PIO_IT_FALL_EDGE,  but_callBack);
	
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 0);

	desligaValvula();
	
	// super loop
	// aplicacoes embarcadas no devem sair do while(1).
	while (1) {

		int y = pio_get(SENSOR_PIO, PIO_INPUT, SENSOR_PIO_PIN_MASK);

		//Botao como interrupitor(on/of) e debouncing.
		if(but_flag){
			delay_ms(200);
			acionamento = !acionamento;
		
			but_flag = false;
		 }
		 
		//Verifica se o sistema esta ligado, caso esteja le o sensor, controlando o estado da valvula.
		if(acionamento){
			ligaLED(LED_VERDE_PIO, LED_VERDE_PIO_PIN_MASK);
			
			if(y){
				ligaLED(LED_VERMELHO_PIO, LED_VERMELHO_PIO_PIN_MASK);
				ligaValvula();
			}
			else{
				desligaLED(LED_VERMELHO_PIO, LED_VERMELHO_PIO_PIN_MASK);
				desligaValvula();
			}
		}

		//Desliga o sistema
		else{
			desligaValvula();
			desligaLED(LED_VERDE_PIO, LED_VERDE_PIO_PIN_MASK);
			desligaLED(LED_VERMELHO_PIO, LED_VERMELHO_PIO_PIN_MASK);
		}
	
		
	}
}