﻿---
# Molha as Pranta
#### author: Leonardo Medeiros e Pedro de la Peña
---

# Projeto 1 - MundoDigital

## Descrição
Este projeto consiste em um regador automatizado que é acionado por um sensor de umidade do solo, este dependendo do nivel de umidade aciona uma valvula de vazão permitindo a passagem de água para o solo

## Materiais
<p>Sensor de umidade</p>
<p>Valvula de vazão solenoide(12V)</p>
<p>Modulo mosfet IRF520N</p>
<p>LED azul e verde</p>
<p>Bateria 9V</p>

## Diagrama

![Img 1](Imagens/Diagrama.png)

## Conexão LED

![Img 2](Imagens/DiagramaLed.png)


## Funcionamento e Implementação dos Componentes

#### Botão 
O botão foi utilizado no periférico PIOA e no pino 19. O botão foi implementado com sistema de interrupção e deboucing, e foi configurado para atuar como interruptor (on/off) do sistema.

#### Sensor Umidade 
O sensor de umidade foi configurado no periférico PIOA e no pino 3. Seu funcionamento consiste em valores binários e retorna 1 caso o solo esteja úmido e 0 caso contrario. A leitura é utilizada para controlar o acionamento da válvula.


#### Micro controlador 
O micro controlador é responsável por integrar e controlar todo o sistema.

#### Válvula
A válvula foi configurada no periférico PIOA e no pino 4. Seu acionamento depende do valor fornecido pelo sensor de umidade.  

#### LED Verde 
O LED verde foi configurado pelo periférico PIOD e ligado no pino 22. O LED verde indica se o sistema esta ativo ou inativo, ou seja, representa visualmente o estado do interruptor (botão).

#### LED Azul 
O LED azul foi configurado pelo periférico PIOC, ligado no pino 12. O LED azul representa visualmente o estado de acionamento da válvula. Caso esteja aceso, a válvula está aberta.

#### Video do Projeto
https://youtu.be/CKm9QFg9M7I

